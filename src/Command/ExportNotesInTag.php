<?php
namespace App\Command;

use App\Lib\NotesExporterByTag;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportNotesInTag extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'notes:tagged';

    protected function configure(): void
    {
      $this->setDescription('Exports notes by tag.')
        ->addArgument('backup-file', InputArgument::REQUIRED, 'The SN backup json file')
        ->addArgument('tag', InputArgument::REQUIRED, "The tag to use to filter the notes. For example, if using the Folders extension: 'Personal.Journal'")
        ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Output directory', 'var/output')
        ->addOption('force-html', 'm', InputOption::VALUE_NONE, 'Force generating notes in HTML format.')
        ->addOption('force-md', 't', InputOption::VALUE_NONE, 'Force generating notes in Markdown format.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $file = $input->getArgument('backup-file');
        $tag = $input->getArgument('tag');

        if (!str_starts_with($file ,'/')) {
            $file = getcwd() . '/' . $file;
        }
        $forceMd = $input->getOption('force-md');
        $forceHtml = $input->getOption('force-html');
        if ($forceHtml && $forceMd) {
            // Show a validation error
            $output->writeln("Options --force-html and --force-md are mutually exclusive.");
            die(1);
        }

        $output->writeln("Using backup in " . $file);

        $data = json_decode(file_get_contents($file), TRUE);
        $exporter = new NotesExporterByTag($data);
        if ($forceHtml) {
            $exporter->setForceHtml($forceHtml);
        }
        if ($forceMd) {
            $exporter->setForceMd($forceMd);
        }

        $ids = $exporter->getNoteIdsIntag($tag);

        $output->writeln(
            sprintf("<info>Found %s number of ids.</info>", count($ids))
        );

        $outputPath = $input->getOption('output');
        if (!str_starts_with($file ,'/')) {
            $file = getcwd() . '/' . $outputPath;
        }

        // Execute!
        $exporter->export($tag, $outputPath);

        return Command::SUCCESS;
    }
}