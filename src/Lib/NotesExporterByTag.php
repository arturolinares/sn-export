<?php
namespace App\Lib;

use App\Lib\NotesExporter;
use Closure;
use Illuminate\Support\Collection;

class NotesExporterByTag extends NotesExporter
{

    /**
     * @param string $tagName
     * @return Collection
     */
    public function getNoteIdsIntag(string $tagName): Collection
    {
        $elements = collect($this->data['items'])
            ->filter($this->isTag($tagName))
            ->map($this->getNoteReferences())
            ->flatten();

        return $elements;
    }

    /**
     * @param string $tagName
     */
    protected function isTag(string $tagName): Closure
    {
        return function($element) use ($tagName) {
            return ($element['content_type'] ?? '') === 'Tag'
                && ($element['content']['title'] ?? '') === $tagName;
        };
    }

    protected function getNoteReferences(): Closure
    {
        return function($tagElement) {
            return collect($tagElement['content']['references'] ?? [])
                ->filter(function ($ref) { return $ref['content_type'] === 'Note'; })
                ->map(function ($ref) { return $ref['uuid'] ?? ''; });
        };
    }

    public function export($tagName, $outputDir)
    {
        $ids = $this->getNoteIdsIntag($tagName);

        $tagSections = explode('.', $tagName);
        foreach ($tagSections as $tagName) {
            $escapedTagName = self::escapeFilename($tagName);
            $outputDir .= '/' . $escapedTagName;

            if (!file_exists($outputDir)) {
                mkdir($outputDir, 0777, true);
            }
        }

        $this->exportByIds($ids->toArray(), $outputDir);
    }
}

