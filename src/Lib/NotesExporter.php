<?php
namespace App\Lib;

use Closure;
use League\CommonMark\CommonMarkConverter;
use League\HTMLToMarkdown\HtmlConverter;

class NotesExporter
{
    /**
     *
     * Forces export in HTML format
     *
     * @var boolean
     */
    private $forceHtml;

    /**
     * Forces the export in Markdown format.
     *
     * @var boolean
     */
    private $forceMd;

    /**
     * @param array $data
     * @return void
     */
    public function __construct(protected array $data)
    { }

    public function exportByIds(array $ids, string $outputPath): void
    {
        collect($this->data['items'])
            ->filter($this->isNote())
            ->filter(function ($el) use ($ids) {
                return in_array($el['uuid'], $ids);
            })
            ->each($this->exportFile($outputPath));
    }

    static public function escapeFilename(string $name): string
    {
        return preg_replace('/[^A-Za-z0-9_\-]/', '_', $name);
    }

    protected function exportFile(string $output): Closure
    {
        return function($rawNote) use ($output) {
            $contents = $rawNote['content']['text'] ?? '';

            $ext = 'txt';
            if (str_starts_with($contents, '<')) {
                $ext = 'html';
            }
            if ($ext === 'txt' && $this->forceHtml) {
                $contents = $this->mdToHtml($contents);
                $ext = 'html';
            }
            if ($ext === 'html' && $this->forceMd) {
                $contents = $this->htmlToMd($contents);
                $ext = 'txt';
            }

            $path = $output . '/' . self::escapeFilename($rawNote['content']['title'])
                . '.' . $ext;

            file_put_contents($path, $contents);
        };
    }

    protected function isNote(): Closure
    {
        return function($element) {
            return ($element['content_type'] ?? '') === 'Note';
        };
    }

    protected function mdToHtml(string $text): string
    {
        // TODO: Inject in constructor.
        $converter = new CommonMarkConverter();
        return $converter->convertToHtml($text);
    }

    /**
     * Convert to Markdown the HTML input.
     */
    protected function htmlToMd(string $text) {
        $converter = new HtmlConverter();
        return $converter->convert($text);
    }

    /**
     * @param boolean $forceHtml
     */
    public function setForceHtml($forceHtml)
    {
        $this->forceHtml = $forceHtml;
        return $this;
    }

    /**
     * @param boolean $forceMd
     */
    public function setForceMd($forceMd)
    {
        $this->forceMd = $forceMd;
        return $this;
    }

}

