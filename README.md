# Standard Notes Export Tool

## Requirements

- PHP v8.x

## How to use it

1. Clone the repository
2. Run this command to export the notes tagged as _Personal_, using a Standard Notes backup stored in `var/backup.json`:

```
bin/sn-export notes:tagged var/backup.json Personal
```
It is possible to convert all notes to Markdown or HTML, see below.

## Usage

```
$ bin/sn-export help notes:tagged

Description:
  Exports notes by tag.

Usage:
  notes:tagged [options] [--] <backup-file> <tag>

Arguments:
  backup-file            The SN backup json file
  tag                    The tag to use to filter the notes. For example, if using the Folders extension: 'Personal.Journal'

Options:
  -o, --output[=OUTPUT]  Output directory [default: "var/output"]
  -m, --force-html       Force generating notes in HTML format.
  -t, --force-md         Force generating notes in Markdown format.
  -h, --help             Display help for the given command. When no command is given display help for the list command
  -q, --quiet            Do not output any message
  -V, --version          Display this application version
      --ansi|--no-ansi   Force (or disable --no-ansi) ANSI output
  -n, --no-interaction   Do not ask any interactive question
  -v|vv|vvv, --verbose   Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```

## Phar

Optionally, a phar is included in the releases page. To use it, replace `bin/sn-export` with the phar file name (A PHP installation is still required):

```
$ sn-export.phar help notes:tagged
```
